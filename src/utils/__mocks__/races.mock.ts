export const raceMock = {
  race_id: '7f4b6f3f-a05d-43ec-9535-9b3f9cb9ecd0',
  race_name: 'Mm Ballarat 3&4Yo Classic',
  race_number: 10,
  meeting_id: '263dad9a-cba6-4a50-8621-0352f9b1aa52',
  meeting_name: 'Ballarat',
  category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae',
  advertised_start: {
    seconds: 1637390400,
  },
  race_form: {
    distance: 1100,
    distance_type: {
      id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
      name: 'Metres',
      short_name: 'm',
    },
    distance_type_id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
    track_condition: {
      id: '11d7e662-487a-11ea-9d54-4689356d4521',
      name: 'Good4',
      short_name: 'good4',
    },
    track_condition_id: '11d7e662-487a-11ea-9d54-4689356d4521',
    weather: {
      id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
      name: 'Overcast',
      short_name: 'overcast',
      icon_uri: 'Overcast',
    },
    weather_id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
    race_comment:
      'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
    generated: 1,
    silk_base_url: 'drr38safykj6s.cloudfront.net',
    race_comment_alternative:
      'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
  },
  venue_id: '26ab0b22-eac2-4378-802e-cbaaa1740afd',
  venue_name: 'Ballarat',
  venue_state: 'VIC',
  venue_country: 'AUS',
}

export const racesMock = [
  {
    race_id: '20bd2ab6-5d45-466e-8daa-7cb651909bcb',
    race_name: 'Bet365 Penshurst Cup (Bm78)',
    race_number: 9,
    meeting_id: '4fe497dc-125c-4266-862a-aa9845d8d57d',
    meeting_name: 'Penshurst',
    category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae',
    advertised_start: {
      seconds: 1637389440,
    },
    race_form: {
      distance: 2000,
      distance_type: {
        id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
        name: 'Metres',
        short_name: 'm',
      },
      distance_type_id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
      track_condition: {
        id: '908a410f-ab10-11e7-85e3-0641c90711b8',
        name: 'Good3',
        short_name: 'good3',
      },
      track_condition_id: '908a410f-ab10-11e7-85e3-0641c90711b8',
      weather: {
        id: '01994c9e-3b74-11e8-a5eb-06a5c6d9a756',
        name: 'Fine',
        short_name: 'fine',
        icon_uri: 'Fine',
      },
      weather_id: '01994c9e-3b74-11e8-a5eb-06a5c6d9a756',
      race_comment:
        "COURT DEEP (5) is a query going to the trip for the first time but the Maher/Eustace yard are the masters of preparing a horse for a middle distance plus. Back with confidence off a close up effort in a good race last start. MANGIONE (6) is building his form nicely this campaign and a win isn't far away. Don't take lightly. TAKUMI (2) is racing well in this distance range and appeals each way. UNDER OATH (3) wasn't far away in a good race last start and is suited at this trip. Include among the chances.",
      generated: 1,
      silk_base_url: 'drr38safykj6s.cloudfront.net',
      race_comment_alternative:
        "COURT DEEP (5) is a query going to the trip for the first time but the Maher/Eustace yard are the masters of preparing a horse for a middle distance plus. Back with confidence off a close up effort in a good race last start. MANGIONE (6) is building his form nicely this campaign and a win isn't far away. Don't take lightly. TAKUMI (2) is racing well in this distance range and appeals each way. UNDER OATH (3) wasn't far away in a good race last start and is suited at this trip. Include among the chances.",
    },
    venue_id: 'ef1e7bb8-e749-49a6-8416-fbde5fae015c',
    venue_name: 'Penshurst',
    venue_state: 'VIC',
    venue_country: 'AUS',
  },
  {
    race_id: '7f4b6f3f-a05d-43ec-9535-9b3f9cb9ecd0',
    race_name: 'Mm Ballarat 3&4Yo Classic',
    race_number: 10,
    meeting_id: '263dad9a-cba6-4a50-8621-0352f9b1aa52',
    meeting_name: 'Ballarat',
    category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae',
    advertised_start: {
      seconds: 1637390400,
    },
    race_form: {
      distance: 1100,
      distance_type: {
        id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
        name: 'Metres',
        short_name: 'm',
      },
      distance_type_id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
      track_condition: {
        id: '11d7e662-487a-11ea-9d54-4689356d4521',
        name: 'Good4',
        short_name: 'good4',
      },
      track_condition_id: '11d7e662-487a-11ea-9d54-4689356d4521',
      weather: {
        id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
        name: 'Overcast',
        short_name: 'overcast',
        icon_uri: 'Overcast',
      },
      weather_id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
      race_comment:
        'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
      generated: 1,
      silk_base_url: 'drr38safykj6s.cloudfront.net',
      race_comment_alternative:
        'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
    },
    venue_id: '26ab0b22-eac2-4378-802e-cbaaa1740afd',
    venue_name: 'Ballarat',
    venue_state: 'VIC',
    venue_country: 'AUS',
  },
  
]

export const summariesMock = {
  '7f4b6f3f-a05d-43ec-9535-9b3f9cb9ecd0': {
    race_id: '7f4b6f3f-a05d-43ec-9535-9b3f9cb9ecd0',
    race_name: 'Mm Ballarat 3&4Yo Classic',
    race_number: 10,
    meeting_id: '263dad9a-cba6-4a50-8621-0352f9b1aa52',
    meeting_name: 'Ballarat',
    category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae',
    advertised_start: {
      seconds: 1637390400,
    },
    race_form: {
      distance: 1100,
      distance_type: {
        id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
        name: 'Metres',
        short_name: 'm',
      },
      distance_type_id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
      track_condition: {
        id: '11d7e662-487a-11ea-9d54-4689356d4521',
        name: 'Good4',
        short_name: 'good4',
      },
      track_condition_id: '11d7e662-487a-11ea-9d54-4689356d4521',
      weather: {
        id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
        name: 'Overcast',
        short_name: 'overcast',
        icon_uri: 'Overcast',
      },
      weather_id: '0b43a420-3b75-11e8-a5eb-06a5c6d9a756',
      race_comment:
        'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
      generated: 1,
      silk_base_url: 'drr38safykj6s.cloudfront.net',
      race_comment_alternative:
        'GLISTENING (10) ran well in a competitive fillies heat and she is good enough to be a threat in a race like this. OUTLAWS REVENGE (2) and AUTHENTIC JEWEL (13) are both improvers second up. The latter won as she liked at Newcastle and while this is tougher she clearly has upside and looks targetted at this. STARRY LEGEND (6) was plain at Flemington but this is the right sort of race for him.',
    },
    venue_id: '26ab0b22-eac2-4378-802e-cbaaa1740afd',
    venue_name: 'Ballarat',
    venue_state: 'VIC',
    venue_country: 'AUS',
  },
  '20bd2ab6-5d45-466e-8daa-7cb651909bcb': {
    race_id: '20bd2ab6-5d45-466e-8daa-7cb651909bcb',
    race_name: 'Bet365 Penshurst Cup (Bm78)',
    race_number: 9,
    meeting_id: '4fe497dc-125c-4266-862a-aa9845d8d57d',
    meeting_name: 'Penshurst',
    category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae',
    advertised_start: {
      seconds: 1637389440,
    },
    race_form: {
      distance: 2000,
      distance_type: {
        id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
        name: 'Metres',
        short_name: 'm',
      },
      distance_type_id: '570775ae-09ec-42d5-989d-7c8f06366aa7',
      track_condition: {
        id: '908a410f-ab10-11e7-85e3-0641c90711b8',
        name: 'Good3',
        short_name: 'good3',
      },
      track_condition_id: '908a410f-ab10-11e7-85e3-0641c90711b8',
      weather: {
        id: '01994c9e-3b74-11e8-a5eb-06a5c6d9a756',
        name: 'Fine',
        short_name: 'fine',
        icon_uri: 'Fine',
      },
      weather_id: '01994c9e-3b74-11e8-a5eb-06a5c6d9a756',
      race_comment:
        "COURT DEEP (5) is a query going to the trip for the first time but the Maher/Eustace yard are the masters of preparing a horse for a middle distance plus. Back with confidence off a close up effort in a good race last start. MANGIONE (6) is building his form nicely this campaign and a win isn't far away. Don't take lightly. TAKUMI (2) is racing well in this distance range and appeals each way. UNDER OATH (3) wasn't far away in a good race last start and is suited at this trip. Include among the chances.",
      generated: 1,
      silk_base_url: 'drr38safykj6s.cloudfront.net',
      race_comment_alternative:
        "COURT DEEP (5) is a query going to the trip for the first time but the Maher/Eustace yard are the masters of preparing a horse for a middle distance plus. Back with confidence off a close up effort in a good race last start. MANGIONE (6) is building his form nicely this campaign and a win isn't far away. Don't take lightly. TAKUMI (2) is racing well in this distance range and appeals each way. UNDER OATH (3) wasn't far away in a good race last start and is suited at this trip. Include among the chances.",
    },
    venue_id: 'ef1e7bb8-e749-49a6-8416-fbde5fae015c',
    venue_name: 'Penshurst',
    venue_state: 'VIC',
    venue_country: 'AUS',
  },
}
