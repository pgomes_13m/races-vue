import { Request } from '../request'
import { RESOURCES } from '../../@types'

describe('Request', () => {
  let instance: Request

  beforeEach(() => {
    instance = new Request('GET', RESOURCES.RACING, {
      method: 'nextraces',
      count: 10,
    })
    instance.baseUrl = 'http://www.test.com'
  })

  it('should build correct resource url', () => {
    const resourceUrl = instance.getResourceUrl()
    expect(resourceUrl).toEqual('http://www.test.com/racing/?method=nextraces&count=10&')
  })

  it('should get the correct request options', () => {
    const requestOptions = instance.getRequestOptions()
    const expected = {
      method: 'GET',
      url: 'http://www.test.com/racing/?method=nextraces&count=10&',
    }
    expect(requestOptions).toEqual(expected)
  })
})
