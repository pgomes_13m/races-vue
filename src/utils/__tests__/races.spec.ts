import { mapRaceIdsToSummaries } from '../races'
import { racesMock, summariesMock } from '../__mocks__/races.mock'

describe('mapRaceIdsToSummaries util', () => {
  const mockRaceIds = [
    '7f4b6f3f-a05d-43ec-9535-9b3f9cb9ecd0',
    '20bd2ab6-5d45-466e-8daa-7cb651909bcb',
  ]

  it('should build correct resource url', () => {
    const actual = mapRaceIdsToSummaries(mockRaceIds as string[], summariesMock)
    expect(actual).toEqual(racesMock)
  })
})
