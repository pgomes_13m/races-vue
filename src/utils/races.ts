
export const mapRaceIdsToSummaries = (raceIds: String[], summaries: Object) => {
  const tempSummaries = Object.values(summaries)
  return raceIds
    //map id to summary
    .map(id => tempSummaries.find(summary => summary.race_id === id))
    // remove null
    .filter(race => !!race)
    // sort by time ascending
    .sort((race1, race2) => race1.advertised_start.seconds - race2.advertised_start.seconds)
}
