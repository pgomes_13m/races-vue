import axios, { AxiosPromise } from 'axios'
import _ from 'lodash'

import { HttpMethodStringType, RequestConfigType, IRequest, ParamsType, HeadersType, RESOURCES } from '@/@types'

class Request implements IRequest {
  baseUrl: string = ''
  resource: RESOURCES = RESOURCES.RACING
  method: HttpMethodStringType = 'GET'
  params: ParamsType
  payload: Object = {}
  headers: HeadersType

  constructor(
    method: HttpMethodStringType = 'GET',
    resource: RESOURCES = RESOURCES.RACING,
    params: ParamsType = {},
    payload: Object = {},
    headers: HeadersType = {}
  ) {
    this.baseUrl = process.env.VUE_APP_API_BASE_URL || ''
    this.resource = resource
    this.method = method
    this.params = params
    this.payload = payload
    this.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      ...headers,
    }
  }

  getRequestOptions() {
    let options: object = {
      url: this.getResourceUrl(),
      method: this.method,
    }

    if (this.method === 'GET') {
      return options
    }

    options = {
      ...options,
      headers: this.headers,
      data: this.payload,
    }

    return options
  }

  getResourceUrl(): string {
    let queryStr: string = ''

    if (!_.isEmpty(this.params)) {
      queryStr = Object.keys(this.params).reduce((acc: string, cur: string) => {
        const value = this.params[cur]
        return `${acc}${cur}=${value}&`
      }, '')
    }

    return `${this.baseUrl}/${this.resource}/?${queryStr}`
  }

  promise(): AxiosPromise {
    const options = this.getRequestOptions()
    return axios(options)
  }
}

const request = (config: RequestConfigType) => {
  const { method, resource, params, payload } = config
  const instance = new Request(method, resource, params, payload)

  return instance.promise()
}

export { Request }

export default request
