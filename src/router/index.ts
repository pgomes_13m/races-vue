//@ts-ignore
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { HomeView } from '@/views'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: HomeView,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
