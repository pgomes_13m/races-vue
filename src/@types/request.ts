import { HTTP_METHODS, RESOURCES } from './enums'

export type HttpMethodStringType = keyof typeof HTTP_METHODS

export type RequestConfigType = {
  baseUrl?: string
  resource: RESOURCES
  headers?: object
  params: ParamsType
  payload?: object
  method?: HttpMethodStringType
}

export interface IRequest {
  getRequestOptions(): void
  getResourceUrl(): String
  promise(): Promise<Object>
}

export type ParamsType = {
  [key: string]: string | number
}

export type HeadersType = {
  [key: string]: string | number
}
