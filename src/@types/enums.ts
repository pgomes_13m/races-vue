export enum HTTP_METHODS {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
}

export enum RESOURCES {
  RACING = 'racing'
}

export enum ACTIONS {
  FETCH_RACES = 'FETCH_RACES',
  FETCH_RACES_SUCCESS = 'FETCH_RACES_SUCCESS',
  FETCH_RACES_ERROR = 'FETCH_RACES_ERROR',
}

export enum MUTATIONS {
  SET_RACES = 'SET_RACES',
  ADD_RACE = 'ADD_RACE',
  DELETE_RACE = 'DELETE_RACE',
}

export enum RACE_CATEGORIES {
  GREYHOUND = 'greyhound',
  HARNESS = 'harness',
  HORSE = 'horse',
}

export enum RACE_CATEGORY_IDS {
  GREYHOUND = '9daef0d7-bf3c-4f50-921d-8e818c60fe61',
  HARNESS = '161d9be2-e909-4326-8c2c-35ed71fb460b',
  HORSE = '4a2788f8-e825-4d36-9894-efd4baf1cfae',
}
