import { IRace } from '@/@types'

export interface IRootState {
  nextRaces: IRace[]
}
