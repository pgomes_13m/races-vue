export interface IRace {
  race_id: string
  race_name: string
  race_number: number
  meeting_id: string
  meeting_name: string
  category_id: string
  advertised_start: AdvertisedStartType
  race_form: RaceFormType
  venue_id: string
  venue_name: string
  venue_state: string
  venue_country: string
}

export type AdvertisedStartType = {
  seconds: number
}

export type RaceFormType = {
  distance: number
  distance_type: InfoType
  distance_type_id: string
  track_condition: InfoType
  track_condition_id: string
  weather: InfoType
}

export type InfoType = {
  id: string
  name: string
  short_name: string
  icon_uri?: string
}

export type TrackConditionType = {}
