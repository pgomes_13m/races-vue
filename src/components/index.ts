import Header from './Header/Header.vue'
import Footer from './Footer/Footer.vue'
import Card from './Card/Card.vue'
import Intro from './Intro/Intro.vue'
import Races from './Races/Races.vue'

export { Header, Footer, Card, Intro, Races }
