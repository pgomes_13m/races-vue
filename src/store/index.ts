import { createStore, createLogger } from 'vuex'

import { store as nextRaces, RacesStoreType, StateType as NextRacesStateType } from '@/store/modules/races'

export type RootStateType = {
  races: NextRacesStateType
}

export type StoreType = RacesStoreType<Pick<RootStateType, 'races'>>

export const store = createStore({
  modules: {
    nextRaces,
  },
})

export function useStore(): StoreType {
  return store as unknown as StoreType
}
