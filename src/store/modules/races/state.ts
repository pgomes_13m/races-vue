import { IRace } from '@/@types'

export type StateType = {
  nextRaces: IRace[]
}

export const state: StateType = {
  nextRaces: [],
}
