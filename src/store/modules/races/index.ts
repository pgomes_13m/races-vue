import { Store as VuexStore, CommitOptions, DispatchOptions, Module } from 'vuex'

import { IRootState } from '@/@types'
import { state, StateType } from './state'
import { getters, GettersType } from './getters'
import { mutations, MutationsType } from './mutations'
import { actions, IActions } from './actions'

export type { StateType }

export type RacesStoreType<S = StateType> = Omit<VuexStore<S>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof MutationsType, P extends Parameters<MutationsType[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<MutationsType[K]>
} & {
  dispatch<K extends keyof IActions>(
    key: K,
    payload: Parameters<IActions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<IActions[K]>
} & {
  getters: {
    [K in keyof GettersType]: ReturnType<GettersType[K]>
  }
}

export const store: Module<StateType, IRootState> = {
  state,
  getters,
  mutations,
  actions,
}
