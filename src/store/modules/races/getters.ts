import { GetterTree } from 'vuex'

import { IRootState, IRace } from '@/@types'
import { StateType } from './state'

export type GettersType = {
  nextRaces(state: StateType): IRace[]
}

export const getters: GetterTree<StateType, IRootState> & GettersType = {
  nextRaces: (state: StateType) => state.nextRaces,
}
