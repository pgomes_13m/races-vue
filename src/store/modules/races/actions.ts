import { ActionTree, ActionContext } from 'vuex'
import _ from 'lodash'

import { request } from '@/utils'
import { StateType } from './state'
import { MutationsType } from './mutations'
import { mapRaceIdsToSummaries } from '@/utils'
import { IRootState, ACTIONS, RequestConfigType, MUTATIONS } from '@/@types'

type AugmentedActionContextType = {
  commit<K extends keyof MutationsType>(key: K, payload: Parameters<MutationsType[K]>[1]): ReturnType<MutationsType[K]>
} & Omit<ActionContext<StateType, IRootState>, 'commit'>

export interface IActions {
  [ACTIONS.FETCH_RACES](
    { commit }: AugmentedActionContextType,
    config: RequestConfigType,
  ): void
}

export const actions: ActionTree<StateType, IRootState> & IActions = {
  async [ACTIONS.FETCH_RACES]({ commit }, config: RequestConfigType) {
    const response = await request(config)
    const { next_to_go_ids = [], race_summaries = {} } = response.data.data

    if (next_to_go_ids.length && !_.isEmpty(race_summaries)) {
      const nextToGoRaces = mapRaceIdsToSummaries(next_to_go_ids, race_summaries)
      commit(MUTATIONS.SET_RACES, nextToGoRaces)
    }
  },
}
