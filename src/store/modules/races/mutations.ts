import { MutationTree } from 'vuex'

import { StateType } from './state'
import { MUTATIONS, IRace } from '@/@types'

export type MutationsType<S = StateType> = {
  [MUTATIONS.SET_RACES](state: S, races: IRace[]): void
}

export const mutations: MutationTree<StateType> & MutationsType = {
  [MUTATIONS.SET_RACES](state: StateType, races: IRace[]) {
    state.nextRaces = races
  },
}
