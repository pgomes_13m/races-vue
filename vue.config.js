const webpack = require('webpack')
const processedPublicPath = process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME ? `/${process.env.CI_PROJECT_NAME}/` : '/'

module.exports = {
  publicPath: processedPublicPath,
  configureWebpack: () => {
    const publicPathPublished = new webpack.DefinePlugin({
      'process.env.VUE_APP_PUBLIC_PATH': JSON.stringify(processedPublicPath),
    })

    return {
      plugins: [ publicPathPublished ],
    }
  },
}
