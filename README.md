# races-vue

Vue app to display the races as per the categories

## Demo
https://pgomes_13m.gitlab.io/races-vue/

Note - if the Neds API can allow access to the above url, you can see the races. 
If not, then you would need to clone the repo & run the app locally.

## Synopsis

The app makes use of following open source projects -

- [Vue] - The Progressive JavaScript Framework
- [Typescript] - A strongly typed programming language that builds on Javascript.
- [Vuex] - Vuex is a state management pattern + library for Vue.js applications
- [Webpack] - bundle your assets
- [Bootstrap5] - World’s most popular framework for building responsive, mobile-first sites
- [Jest] - Delightful JavaScript Testing

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

Note - there are some issues with the Vue 3 Jest package (@vue/vue3-jest) which fails in component testing. Need more time to investigate.

## Contributors

[Pascoal Gomes](https://au.linkedin.com/in/pascoal-gomes-a4835954)

[vue]: https://vuejs.org/
[typescript]: https://www.typescriptlang.org/
[vuex]: https://vuex.vuejs.org/
[webpack]: https://webpack.js.org//
[bootstrap5]: https://getbootstrap.com/docs/5.0/getting-started/introduction/
[jest]: https://jestjs.io/
