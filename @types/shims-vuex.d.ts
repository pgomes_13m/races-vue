import { StoreType } from '../src/store'

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: StoreType
  }
}
