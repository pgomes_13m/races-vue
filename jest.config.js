module.exports = {
  transform: {
    '^.+\\.vue$': '@vue/vue3-jest',
    '^.+\\.tsx?$': 'ts-jest',
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  moduleFileExtensions: ['js', 'ts', 'tsx', 'json', 'vue'],
  testMatch: ['<rootDir>/src/**/__tests__/*.spec.ts', '<rootDir>/src/**/__tests__/*.spec.tsx'],
}
